/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stream;

import java.util.function.IntUnaryOperator;
import java.util.stream.IntStream;

/**
 *
 * @author orion
 */
public class Main {
    public static void main(String[] args) {
        
//        IntUnaryOperator unary = new IntUnaryOperator() {
//            @Override
//            public int applyAsInt(int i) {
//                return i * 2;
//            }
//        };
        
//        IntUnaryOperator unary = (int i) -> {return i * 2;};
        
//        IntUnaryOperator unary = i -> i * 2;;
            

        IntStream.rangeClosed(1, 10).map(i -> i *2).forEach(System.out::println);
        
//        System.out.println("impŕimir num "+IntStream.rangeClosed(1, 10).map(unary).sum());
    }
}
