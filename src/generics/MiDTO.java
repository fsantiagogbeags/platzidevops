/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generics;

/**
 *
 * @author orion
 */
public class MiDTO {
    private String nombre;
    private String apelldos;

    public MiDTO() {
    }

    public MiDTO(String nombre) {
        this.nombre = nombre;
    }

    public MiDTO reemplazarNombre(String nombre){
        this.nombre = nombre;
        return this;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApelldos() {
        return apelldos;
    }

    public void setApelldos(String apelldos) {
        this.apelldos = apelldos;
    }

    @Override
    public String toString() {
        return "MiDTO{" + "nombre=" + nombre + ", apelldos=" + apelldos + '}';
    }
    
    
}
