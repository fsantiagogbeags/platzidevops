/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generics;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author orion
 */
public class Generics {

    public static void main(String[] args) {
        var array = new Integer[]{6, 3, 1, 7};
        Stream.of(ordenar(array)).forEach(System.out::println);
    }
     

    public static <T> T[] ordenar(T[] t) {
        return Stream.of(t).sorted().collect(Collectors.toList()).toArray(t);
    }

    public static Integer[] ordenar(Integer[] arreglo) {
        return Stream.of(arreglo).sorted().toArray(Integer[]::new);
    }

    public static Double[] ordenar(Double[] arreglo) {
        return Stream.of(arreglo).sorted().toArray(Double[]::new);
    }

    public static Short[] ordenar(Short[] arreglo) {
        return Stream.of(arreglo).sorted().toArray(Short[]::new);
    }

    public static Float[] ordenar(Float[] arreglo) {
        return Stream.of(arreglo).sorted().toArray(Float[]::new);
    }


}
